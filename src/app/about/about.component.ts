import { Component, OnInit, Renderer2, OnDestroy, HostListener } from '@angular/core';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent implements OnInit, OnDestroy {
  isShow: boolean;
  topPosToStartShowing = 100;
  constructor(
    private renderer: Renderer2
  ) { }

  ngOnInit() {
    window.scrollTo(0, 0);
    this.renderer.addClass(document.body, 'white-bg');
    this.renderer.addClass(document.body, 'about-page');
  }

  ngOnDestroy() {
    this.renderer.removeClass(document.body, 'white-bg');
    this.renderer.removeClass(document.body, 'about-page');
  }

  @HostListener('window:scroll')
  checkScroll() {
      
    // window의 scroll top
    // Both window.pageYOffset and document.documentElement.scrollTop returns the same result in all the cases. window.pageYOffset is not supported below IE 9.

    const scrollPosition = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop || 0;

    console.log('[scroll]', scrollPosition);
    
    if (scrollPosition >= this.topPosToStartShowing) {
      this.isShow = true;
    } else {
      this.isShow = false;
    }
  }

  // TODO: Cross browsing
  gotoTop() {
    window.scroll({ 
      top: 0, 
      left: 0, 
      behavior: 'smooth' 
    });
  }

}
