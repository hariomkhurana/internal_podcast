import { Injectable } from '@angular/core';
import { NetworkService } from '../shared/network.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private networkService: NetworkService) { }
  validateLogin(req: any) {
    return this.networkService.login("api/operator/login", req, null, null);
  }

  saveOperator(request:any) {
    return this.networkService.login("api/operator", request, null, null);
  }

  verifyOperator(request:any) {
    return this.networkService.post("api/operator/verify", request, null, null);
  }
  createOTP(request:any) {
    return this.networkService.post("api/otp/create", request, null,"bearer");
  }
  verifyOTP(request:any) {
    return this.networkService.post("api/otp/verify", request, null,"bearer");
  }
  uploadImage(image: File) {
    const formData = new FormData();

    formData.append("image", image);
    return this.networkService.uploadImages("api/s3upload/image-upload", formData, null, "bearer");
  }
 
}
