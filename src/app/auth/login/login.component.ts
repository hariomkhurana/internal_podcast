import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CommonService } from '../../shared/common.service';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  isLogin:boolean = false;
  isOpt:boolean = false ;
  validOTP: boolean = false;
  loginForm: FormGroup;
  signUpForm:FormGroup
  formSubmitted: boolean = false;
  errorMsg: string = "Ok";
  headers = [];
  adminUser: boolean = true;
  clubUser: boolean = false;
  LoginFlag: boolean = true;
  ResetFlag: boolean = false;
  Loginmessage: string = "";
  isLoading: boolean = false;
  isMobile: boolean = true;
  otp: any ;
  optToken: any ;
  emailPattern = /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)+$/;
  phoneNumber :any
  email:any;
  errorCount: number = 0;
  selectedFile: File;
  CACDocumentUrl:any;
  constructor(private fb: FormBuilder,
    private authServices: AuthService,
    private commonService: CommonService,
    private router: Router
    ) { }

  ngOnInit() {
    this.initialiseForms();
    localStorage.clear();
  }
  initialiseForms() {
    this.loginForm = this.fb.group({
      email: ["", [Validators.required,Validators.pattern(this.emailPattern)]],
      password: ["", [Validators.required]]
    });
    this.signUpForm = this.fb.group({
       email:['', [Validators.required, Validators.pattern(this.emailPattern)]],
       password: [null, [Validators.required, Validators.minLength(6)]],
       mobile: ["", [Validators.required, Validators.minLength(10)]]
    });
  }
  

  inputNumber(event){
    let mobile= this.signUpForm.value.mobile;
    let first =mobile.charAt(0)
     if(first == "0") {
      this.signUpForm.controls['mobile'].setValidators([ Validators.minLength(11),Validators.required,,Validators.maxLength(11)]);
      this.signUpForm.controls['mobile'].updateValueAndValidity();
     }
     else{
      this.signUpForm.controls['mobile'].setValidators([ Validators.minLength(10),Validators.required,,Validators.maxLength(10)]);
      this.signUpForm.controls['mobile'].updateValueAndValidity();
     }
    
    }

    
    public inputValidator(event: any) {
      //console.log(event.target.value);
      const pattern = /^[0-9]*$/;
      //let inputChar = String.fromCharCode(event.charCode)
      if (!pattern.test(event.target.value)) {
        event.target.value = event.target.value.replace(/[^a-zA-Z]/g, "");
        // invalid character, prevent input
  
      }
    }
  navigateLogin(value:any) {
    
    if(value =='login') {
      this.isLogin =false;
      this.isOpt = false;
      this.loginForm.reset();
      this.signUpForm.reset();

    }
     if(value=='signup') {
      this.isLogin = true;
      this.isOpt = false;
      this.loginForm.reset();
      this.signUpForm.reset();
     }
     if (value=='otp') {
      this.isOpt = true;
     }
     if (value=='back') {
      this.isLogin = false;
      this.isOpt = false;
     }
  }

  loginFN() {
    this.router.navigate(['main/trips-management/view-trips'])
  }

  onOtpChange(event): void {
    if(event) {
      this.otp = event;
      if(this.otp.length == 4) {
        this.validOTP = true;
      }
      else {
        this.validOTP = false;
      }
    }
  }
  login() {
    this.formSubmitted = true;
    if (this.loginForm.valid) { 
      let req = {
        email: this.loginForm.controls["email"].value,
        password: this.loginForm.controls["password"].value
      };
      this.isLoading = true;
      this.authServices.validateLogin(req).subscribe((res: HttpResponse<any>) => {
        this.isLoading = false;
        if ((res["message"] = "Success")) {
          this.commonService.setStorage("token", res.headers.get('Authorization'));
          let userData = this.commonService.getUser();
          localStorage.setItem('side0909',"0");
          this.router.navigate(["main/dashboard"])
        } else {
        }
      },
        err => {
          this.isLoading = false;
        });

    }


  }

  onlyNumberKey(event) {
    return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
  }
  onlyAlphabets(e) {
        
    return (e.charCode > 64 && e.charCode < 91) || (e.charCode > 96 && e.charCode < 123) || e.charCode == 32 || e.charCode == 45;
  }
  navigatetoHome() {
    // window.open("http://Maap.com.ng/","_self")

  }

  OperatorSubmit() {
    this.isLoading = true;
    let request = this.signUpForm.value;

      request['otpToken'] = this.optToken;
        this.authServices.saveOperator(request).subscribe(
          res => {
            this.isLoading = false;
            if (res['body']["message"] == "Success") {

              this.commonService.setStorage("token", res.headers.get('Authorization'));
              let userData = this.commonService.getUser();
              localStorage.setItem('UpdSt','JkIIC');
              localStorage.setItem('Upddl', JSON.stringify(res.body.data));
              this.router.navigate(["main/account/detail"])

              this.signUpForm.reset();
            } else {
            }
            this.isLoading = false;
          },
          err => {
            this.isLoading = false;
          }
        );
    
    }

  verifyOperator() {
      this.isLoading = true;
      let request = {
        "mobile": this.signUpForm.value.mobile,
        "email": this.signUpForm.value.email
    }

      this.authServices.verifyOperator(request).subscribe(
        res => {
          console.log('response verifyOperator ',res)
          this.isLoading = false;
          if (res["message"] == "Success") {
             this.generateOPT();
          } else {
          }
          this.isLoading = false;
        },
        err => {
          this.isLoading = false;
        }
      );
    }


    onFileChanged(event) {
      this.isLoading = true;
      this.selectedFile = event.target.files[0];
      
      this.authServices.uploadImage(this.selectedFile).subscribe(res => {
        this.isLoading = false;
        if (res["message"] === "Success") {
          this.CACDocumentUrl = res.data.url;
        } 
      } ,err => {
        this.isLoading = false;
      });
    }

  generateOPT() {
      this.isLoading = true;
      let request = {
        "mobile": this.signUpForm.value.mobile,
        "type":'OR'
    }

    this.mobileSubString();
    this.isLoading = false;
      this.authServices.createOTP(request).subscribe(
        res => {
          console.log(res,'opt Response.')
          this.isLoading = false;
          if (res["message"] == "Success") { 
              this.isOpt = true;
              this.mobileSubString();
          } else {
          }
          this.isLoading = false;
        },
        err => {
          console.log(err,'opt Response. Error')
          this.isLoading = false;
        }
      );
    }
    verifyOPT() {
      this.isLoading = true;
      let request = {
     "mobile": this.signUpForm.value.mobile,
     //  "email": this.signUpForm.value.email,
        otp: parseInt(this.otp),
        "type":'OR'
    }
    ;
      this.authServices.verifyOTP(request).subscribe(
        res => {
          this.isLoading = false;
          if (res["message"] == "Success") {
            this.optToken =res.data.token;
            this.OperatorSubmit();
          } else {
          }
          this.isLoading = false;
        },
        err => {

          if( this.errorCount == 2) {
            this.navigateLogin('signup')
          }
          this.errorCount = this.errorCount + 1;
          this.isLoading = false;
        }
      );
    }
    mobileSubString() {
      let length =  this.signUpForm.value.mobile.length;
      let mobile =  this.signUpForm.value.mobile;
      let email =  this.signUpForm.value.email;
      let emailLengh = this.signUpForm.value.email.length;
      this.phoneNumber =  mobile.substring(0,3) + '***'+ mobile.substring(length-4,length);
      this.email = email.substring(0,5) +"*****" + email.substring(emailLengh-4,emailLengh);
    }

    submitOperator(){
      if (this.signUpForm.valid) {
           this.verifyOperator();
      }
    }


} 
