import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login.component';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { SpinnerComponent } from '../../shared/spinner/spinner.component';
import { SharedModule } from '../../shared/shared.module';
import { NgOtpInputModule } from  'ng-otp-input';
import { LgFooterComponent } from '../../lg-footer/lg-footer.component';

const router: Routes = [
  { path: '', component: LoginComponent },
  { path: "login", component: LoginComponent },
];

@NgModule({
  imports: [CommonModule,SharedModule, RouterModule.forChild(router), FormsModule, ReactiveFormsModule, HttpClientModule,NgOtpInputModule],
  declarations: [LoginComponent,LgFooterComponent]
})
export class LoginModule { }
