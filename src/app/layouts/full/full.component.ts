import * as $ from 'jquery';
import { MediaMatcher } from '@angular/cdk/layout';
import { Router } from '@angular/router';
import {
  ChangeDetectorRef,
  Component,
  NgZone,
  OnDestroy,
  ViewChild,
  HostListener,
  Directive,
  AfterViewInit
} from '@angular/core';

import { AppHeaderComponent } from './header/header.component';
import { AppSidebarComponent } from './sidebar/sidebar.component';
import { MenuItems } from '../../shared/menu-items/menu-items';
import { CommonService } from '../../shared/common.service';

/** @title Responsive sidenav */
@Component({
  selector: 'app-full-layout',
  templateUrl: 'full.component.html',
  styleUrls: ['full.component.scss']
})
export class FullComponent implements OnDestroy, AfterViewInit {
  mobileQuery: MediaQueryList;
  isMenuOpen = true;
  contentMargin: number ;
  private _mobileQueryListener: () => void;

  MENUITEMS = [
    { state: 'dashboard', name: 'Dashboard', iconUrl:'assets/images/dashboard_white.png',iconUrlActive:'assets/images/dashboard_color.png'},
    { state: 'search', name: 'Search', iconUrl:'assets/images/search_white.png',iconUrlActive:'assets/images/search_color.png'},
    { state: 'message', name: 'Message', iconUrl:'assets/images/message_white.png',iconUrlActive:'assets/images/message_color.png'},
    { state: 'match', name: 'My Matches', iconUrl:'assets/images/match_white.png',iconUrlActive:'assets/images/match_color.png'},
    { state: 'profile', name: 'My Profile', iconUrl:'assets/images/profile_white.png',iconUrlActive:'assets/images/profile_color.png'},
    { state: 'subscription', name: 'Subscription', iconUrl:'assets/images/sub_white.png',iconUrlActive:'assets/images/sub_color.png'},
    { state: 'favourite', name: 'My Favourite', iconUrl:'assets/images/fav_white.png',iconUrlActive:'assets/images/fav_color.png'},

       ];
       MENUACCOUNT = [
        { state: 'setting', name: 'Setting', iconUrl:'assets/images/setting_white.png',iconUrlActive:'assets/images/setting_color.png'},
        { state: 'term', name: 'Terms & Condition', iconUrl:'assets/images/term_white.png',iconUrlActive:'assets/images/term_color.png'},
           ];

  constructor(
    changeDetectorRef: ChangeDetectorRef,
    media: MediaMatcher,
    public menuItems: MenuItems,
    public common :CommonService
  ) {
    this.mobileQuery = media.matchMedia('(min-width: 768px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);
    console.log('mobileQuery',this.mobileQuery)
  }

  WindowResize(e) {
    // setTimeout(() => { window.dispatchEvent(new Event('resize')); }, 260);
    }

  ngOnDestroy(): void {
    this.mobileQuery.removeListener(this._mobileQueryListener);
  }
  activeRoute(index) {
    console.log(index)
    localStorage.setItem('side0909',index);
    localStorage.setItem('side0908',"-1");
  }
  activeSetting(index){
    console.log(index)
    localStorage.setItem('side0909',"-1");
    localStorage.setItem('side0908',index);
  }
  readLocalStorageValue(key: string): number {
    return parseInt(localStorage.getItem(key));
  }
  logOut() {
    this.common.logOut();
  }
  ngAfterViewInit() {}
}
