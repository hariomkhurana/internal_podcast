import { Component, AfterViewInit, OnInit } from '@angular/core';
import { DashboardService } from './dashboard.service';

@Component({
	selector: 'app-dashboard',
	templateUrl: './dashboard.component.html',
	styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
	isLoading: boolean=false;
	stateList: [] ;
	stateObj:any;
	stateId: 'all';
	dashCount :any;
	
	constructor( 
	private DashboardService:DashboardService
			) { }

	ngOnInit() {
		//this.isLoading = true;
	  //this.getDashboard();
	  this.stateId = 'all';
	}

 
	getDashboard() {
		this.isLoading = true;
		this.DashboardService.dashboardCount().subscribe (
		  res => {
			this.isLoading = false;
			if ((res["message"] = "Success")) {
			  this.dashCount = res["data"]
			} else {
			  
			}
			this.isLoading = false;
		  },
		  err => {
			this.isLoading = false;
		  }
		);
	  }

}
