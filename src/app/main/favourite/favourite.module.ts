import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { DemoMaterialModule } from '../../demo-material-module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FavouriteComponent } from './favourite.component';
import { FavouriteRoutes } from './favourite.routing';
import { GoogleChartsModule } from 'angular-google-charts';
import { SharedModule } from '../../shared/shared.module';
import { FavouriteService } from './favourite.service';
@NgModule({
  imports: [
    CommonModule,
    DemoMaterialModule,
    FlexLayoutModule,
    SharedModule,
    GoogleChartsModule,
    RouterModule.forChild(FavouriteRoutes)
  ],
  declarations: [FavouriteComponent],
  providers:[FavouriteService]
})
export class FavouriteModule {}
