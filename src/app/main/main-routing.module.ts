import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainComponent } from './main.component';
import { AuthGuardMain } from '../auth/auth-guard.service';


const routes: Routes = [
  {
    path: "main",
    component: MainComponent,
    canActivate: [AuthGuardMain],
    children: [
       { path: 'dashboard', loadChildren: () => import('./dashboard/dashboard.module').then(m => m.DashboardModule)},
       { path: 'search', loadChildren: () => import('./search/search.module').then(m => m.SearchModule)},
       { path: 'message', loadChildren: () => import('./message/message.module').then(m => m.MessageModule)},
       { path: 'match', loadChildren: () => import('./match/match.module').then(m => m.MatchModule)},
       { path: 'profile', loadChildren: () => import('./profile/profile.module').then(m => m.ProfileModule)},
       { path: 'favourite', loadChildren: () => import('./favourite/favourite.module').then(m => m.FavouriteModule)},
       { path: 'subscription', loadChildren: () => import('./subsciption/subsciption.module').then(m => m.SubModule)},
       { path: 'setting', loadChildren: () => import('./setting/setting.module').then(m => m.SettingModule)},
       { path: 'term', loadChildren: () => import('./terms/term.module').then(m => m.TermModule)},
      



         ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MainRoutingModule { }
