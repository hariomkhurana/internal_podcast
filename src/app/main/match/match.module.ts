import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { DemoMaterialModule } from '../../demo-material-module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatchComponent } from './match.component';
import { MatchRoutes } from './match.routing';
import { GoogleChartsModule } from 'angular-google-charts';
import { SharedModule } from '../../shared/shared.module';
import { MatchService } from './match.service';
@NgModule({
  imports: [
    CommonModule,
    DemoMaterialModule,
    FlexLayoutModule,
    SharedModule,
    GoogleChartsModule,
    RouterModule.forChild(MatchRoutes)
  ],
  declarations: [MatchComponent],
  providers:[MatchService]
})
export class MatchModule {}
