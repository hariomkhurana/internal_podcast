import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { DemoMaterialModule } from '../../demo-material-module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ProfileComponent } from './profile.component';
import { ProfileRoutes } from './profile.routing';
import { GoogleChartsModule } from 'angular-google-charts';
import { SharedModule } from '../../shared/shared.module';
import { ProfileService } from './profile.service';
@NgModule({
  imports: [
    CommonModule,
    DemoMaterialModule,
    FlexLayoutModule,
    SharedModule,
    GoogleChartsModule,
    RouterModule.forChild(ProfileRoutes)
  ],
  declarations: [ProfileComponent],
  providers:[ProfileService]
})
export class ProfileModule {}
