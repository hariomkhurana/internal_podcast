import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { DemoMaterialModule } from '../../demo-material-module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { SearchComponent } from './search.component';
import { SearchRoutes } from './search.routing';
import { GoogleChartsModule } from 'angular-google-charts';
import { SharedModule } from '../../shared/shared.module';
import { SearchService } from './search.service';
@NgModule({
  imports: [
    CommonModule,
    DemoMaterialModule,
    FlexLayoutModule,
    SharedModule,
    GoogleChartsModule,
    RouterModule.forChild(SearchRoutes)
  ],
  declarations: [SearchComponent],
  providers:[SearchService]
})
export class SearchModule {}
