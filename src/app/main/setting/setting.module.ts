import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { DemoMaterialModule } from '../../demo-material-module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { SettingComponent } from './setting.component';
import { SettingRoutes } from './setting.routing';
import { GoogleChartsModule } from 'angular-google-charts';
import { SharedModule } from '../../shared/shared.module';
import { SettingService } from './setting.service';
@NgModule({
  imports: [
    CommonModule,
    DemoMaterialModule,
    FlexLayoutModule,
    SharedModule,
    GoogleChartsModule,
    RouterModule.forChild(SettingRoutes)
  ],
  declarations: [SettingComponent],
  providers:[SettingService]
})
export class SettingModule {}
