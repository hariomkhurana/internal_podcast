import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { DemoMaterialModule } from '../../demo-material-module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { SubsciptionComponent } from './subsciption.component';
import { SubRoutes } from './subsciption.routing';
import { GoogleChartsModule } from 'angular-google-charts';
import { SharedModule } from '../../shared/shared.module';
import { SubsciptionService } from './subsciption.service';
@NgModule({
  imports: [
    CommonModule,
    DemoMaterialModule,
    FlexLayoutModule,
    SharedModule,
    GoogleChartsModule,
    RouterModule.forChild(SubRoutes)
  ],
  declarations: [SubsciptionComponent],
  providers:[SubsciptionService]
})
export class SubModule {}
