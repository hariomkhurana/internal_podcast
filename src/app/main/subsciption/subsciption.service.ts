import { Injectable } from '@angular/core';
import { NetworkService } from '../../shared/network.service';

@Injectable({
  providedIn: 'root'
})
export class SubsciptionService {

  constructor(private networkservice:NetworkService) { }

  dashboardCount() {
    return this.networkservice.get("api/dashboard/",null,null,'bearer')
  }

}
