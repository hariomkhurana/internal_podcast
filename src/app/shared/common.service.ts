import { Injectable, Injector } from "@angular/core";
import { Constants } from "./common.const";
import { Router } from "@angular/router";
import { HttpHeaders } from "@angular/common/http";
import * as jwt_decode from "jwt-decode";
import { IfStmt } from "@angular/compiler";

@Injectable()
export class CommonService extends Constants {
  public httpOptions = {};

  constructor(private injector: Injector) {
    super();
  }

  public get router(): Router {
    return this.injector.get(Router);
  }

  getToken(endPoint: any) {
    let token = localStorage.getItem("token");
    let operatorData = JSON.parse(localStorage.getItem('Upddl'));
    let code ="";
    if (operatorData) {
      code = operatorData.operatorCode ;
    }
    else  
    {
      code = 'NA' ;
    }

    console.log('OP',operatorData);
    switch (endPoint) {
      case this.basic:
        this.httpOptions = {
          headers: new HttpHeaders({
            "Content-Type": "application/json",
           // "Authorization": token,
            "operatorCode" : code
          })
        };
        if(token) {
          this.httpOptions = {
            headers: new HttpHeaders({
              "Content-Type": "application/json",
             "Authorization": token,
             "operatorCode" :code
            })
          };
        }
        break;
      case this.bearer:
        this.httpOptions = {
          headers: new HttpHeaders({
            "Content-Type": "application/json",
            "operatorCode" :code
          })
        };
        if(token) {
          this.httpOptions = {
            headers: new HttpHeaders({
              "Content-Type": "application/json",
             "Authorization": token,
             "operatorCode" :code
            })
          };
        }
        break;
      default:
        break;
    }
    return this.httpOptions;
  }
  getToken1(endPoint: any) {
    let token = localStorage.getItem("token");
    let operatorData = JSON.parse(localStorage.getItem('Upddl'));
    let code ="";
    if (operatorData) {
      code = operatorData.operatorCode ;
    }
    else  
    {
      code = 'NA' ;
    }

    console.log('OP',operatorData);
    switch (endPoint) {
      case this.basic:
        this.httpOptions = {
          headers: new HttpHeaders({
          "Content-Type": "multipart/form-data; boundary=----WebKitFormBoundaryDDyd8IcmRdEbP2p7",
          "Authorization":token,
          "operatorCode":operatorData.operatorCode
          })
        };
        if(token) {
          this.httpOptions = {
            headers: new HttpHeaders({
              "Content-Type": "multipart/form-data; boundary=----WebKitFormBoundaryDDyd8IcmRdEbP2p7",
              "Authorization":token,
              "operatorCode":operatorData.operatorCode
            })
          };
        }
        break;
      case this.bearer:
        this.httpOptions = {
          headers: new HttpHeaders({
            "Content-Type": "multipart/form-data; boundary=----WebKitFormBoundaryDDyd8IcmRdEbP2p7",
            "Authorization":token,
            "operatorCode":operatorData.operatorCode
          })
        };
        if(token) {
          this.httpOptions = {
            headers: new HttpHeaders({
              "Content-Type": "multipart/form-data; boundary=----WebKitFormBoundaryDDyd8IcmRdEbP2p7",
              "Authorization":token,
              "operatorCode":operatorData.operatorCode
            })
          };
        }
        break;
      default:
        break;
    }
    return this.httpOptions;
  }

  setStorage(key, value): any {
    if (typeof value == "object") {
      value = JSON.stringify(value);
    }
    localStorage.setItem(key, value);
  }

  getStorage(key): any {
    const x = localStorage.getItem(key);
    try {
      const xx = JSON.parse(x);
      return xx;
    } catch (e) {
      return x;
    }
  }
  removeStorage(key): any {
    localStorage.removeItem(key);
  }

  getUser() {
    const token = this.getStorage("token");
    const userData = jwt_decode(token);
    return userData;
  }

  isAuthenticated() {
    if (localStorage.getItem("token") == null) {
      return false;
    } else {
      return true;
    }
  }

  logOut() {
    localStorage.clear();
    this.router.navigate(["/auth/login"]);
  }
}
