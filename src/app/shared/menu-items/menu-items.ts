import { Injectable } from "@angular/core";

export interface Menu {
  state: string;
  name: string;
  type: string;
  icon: string;
}

const MENUITEMS = [
 { state: 'dashboard', type: 'link', name: 'Dashboard', icon: 'account_box', iconColor: '#FF5959',show: true ,iconUrl:'assets/images/dashboard.png' },
  { state: 'account', type: 'link', name: 'Account', icon: 'account_box', iconColor: '#FF5959',show: true ,iconUrl:'assets/images/ic_account.png'},
  { state: 'bus', type: 'link', name: 'Manage Bus', icon: 'directions_car' , iconColor: '#FF5959',iconUrl:'assets/images/manageBus.png' ,show: true },
  { state: 'stop', type: 'link', name: 'Manage Park', icon: 'directions_car' , iconColor: '#FF5959',iconUrl:'assets/images/busStop.png' ,show: true },
  { state: 'vehicle', type: 'link', name: 'Vehicle', icon: 'directions_car' , iconColor: '#FF5959',iconUrl:'assets/images/ic_vehicle.png' ,show: true },
  { state: 'driver', type: 'link', name: 'Driver/Assistant', icon: 'directions_car' , iconColor: '#FF5959',iconUrl:'assets/images/driver.png' ,show: true },
   { state: 'seller', type: 'link', name: 'Ticket Sellers', icon: 'directions_car' , iconColor: '#FF5959',iconUrl:'assets/images/staff.png' ,show: true },
  
  // { state: 'trip', type: 'link', name: 'Manage Roots', icon: 'directions_car' , iconColor: '#FF5959',iconUrl:'assets/images/create_trip.png' ,show: true },
  // { state: 'terminal', type: 'link', name: 'Terminal/Bus Park', icon: 'directions_car' , iconColor: '#FF5959',iconUrl:'assets/images/terminal.png' ,show: true },
  // { state: 'payment-management/view-payment', type: 'link', name: 'Invoice', icon: 'subtitles', iconColor: '#FF5959' ,iconUrl:'assets/images/ic_invoice.png',show: true  },
  // { state: 'trips-management/view-trips', type: 'link', name: 'Trips', icon: 'date_range', iconColor: '#FF5959',iconUrl:'assets/images/ic_trips.png',show: true  },
  { state: 'trip', type: 'link', name: 'Trips', icon: 'date_range', iconColor: '#FF5959',iconUrl:'assets/images/ic_trips.png',show: true  },
  { state: 'booking', type: 'link', name: 'Bookings', icon: 'date_range', iconColor: '#FF5959',iconUrl:'assets/images/booking.png',show: true  },
  { state: 'password', type: 'link', name: 'Change Password', icon: 'flare' , iconColor: '#FF5959', iconUrl:'assets/images/ic_change_password.png',show: true },
 // { state: 'shareholder', type: 'link', name: 'Share Holder', icon: 'directions_car', iconColor: '#FF5959',iconUrl:'assets/images/ic_invoice.png', show: true  }
];

@Injectable()
export class MenuItems {
  getMenuitem(): Menu[] {
    return MENUITEMS;
  }
}
